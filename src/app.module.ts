import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './modules/user/user.module';
import { UserController } from './modules/user/user.controller';
import { QuestionService } from './question/question.service';
import { QuestionController } from './question/question.controller';

@Module({
  imports: [UserModule, DatabaseModule],
  controllers: [AppController, UserController, QuestionController],
  providers: [AppService, QuestionService],
})
export class AppModule {}
