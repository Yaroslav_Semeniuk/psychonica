import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Category } from '../modules/category/category.entity';
import { GenderEnum } from '../shared/enums/gender.enum';
import { User } from '../modules/user/user.entity';

@Entity()
export class Question {
  @PrimaryGeneratedColumn()
  readonly id: string;

  @Column()
  titleText: string;

  @Column()
  descriptionText: string;

  @Column()
  imgSrc: string;

  @Column()
  @ManyToOne(() => Category, (category) => category.name)
  category: Category;

  @Column()
  gender: GenderEnum;

  @Column()
  @ManyToOne(() => User, (user) => user.id) // проверить работоспос.
  authorId: string;

  // @Column()
  // answer: Answer
}
