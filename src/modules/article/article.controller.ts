import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ArticleService } from './article.service';
import { Article } from './article.entity';

@Controller('article')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get()
  getArticles(): Promise<Article[]> {
    return this.articleService.getAll();
  }

  @Get(':id')
  getArticleById(@Param('id') id: string): Promise<Article> {
    return this.articleService.getById(id);
  }

  @Get(':id')
  getRole(@Param('role') role: string): Promise<Article> {
    return this.articleService.getRole(role);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  createArticle(@Body() article: Article): Promise<Article> {
    return this.articleService.createOne(article);
  }

  @Put(':id')
  updateArticle(
    @Body() article: Article,
    @Param('id') id: string,
  ): Promise<Article> {
    return this.articleService.update(id, article);
  }

  @Delete(':id')
  deleteArticle(@Param('id') id: string): Promise<Article> {
    return this.articleService.remove(id);
  }
}
