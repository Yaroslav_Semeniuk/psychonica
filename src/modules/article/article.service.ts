import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Article } from './article.entity';

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(Article)
    private readonly articleRepository: Repository<Article>,
  ) {}

  async getAll(): Promise<Article[]> {
    return this.articleRepository.find();
  }

  async getById(id: string): Promise<Article> {
    return await this.articleRepository.findOne(id);
  }

  async getRole(role: string): Promise<Article> {
    return await this.articleRepository.findOne(role);
  }

  async createOne(article: Article): Promise<Article> {
    const newArticle = await this.articleRepository.create(article); // crypto in Node
    await this.articleRepository.save(newArticle);
    return newArticle;
  }

  async update(id: string, article: Article): Promise<Article> {
    await this.articleRepository.update(id, article);
    return await this.articleRepository.findOne(id);
  }

  async remove(id: string): Promise<Article> {
    const article = await this.articleRepository.findOne(id);
    const deleteResponse = await this.articleRepository.delete(article);
    if (deleteResponse.affected) {
      return article;
    }
  }
}
