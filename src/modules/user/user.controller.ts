import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  getAll(): Promise<User[]> {
    return this.userService.getAll();
  }

  @Get(':id')
  getById(@Param('id') id: string): Promise<User> {
    return this.userService.getById(id);
  }

  @Get(':id')
  getRole(@Param('role') role: string): Promise<User> {
    return this.userService.getRole(role);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() user: User): Promise<User> {
    return this.userService.createOne(user);
  }

  @Put(':id')
  update(@Body() userDto: User, @Param('id') id: string): Promise<User> {
    return this.userService.update(id, userDto);
  }

  @Delete(':id')
  delete(@Param('id') id: string): Promise<User> {
    return this.userService.remove(id);
  }
}
